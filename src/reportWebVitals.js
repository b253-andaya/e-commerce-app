// This function takes a callback function as its parameter and reports web vitals data
const reportWebVitals = (onPerfEntry) => {
  // Check if the callback function exists and is a function
  if (onPerfEntry && onPerfEntry instanceof Function) {
    // Import web-vitals library dynamically using ES6 import syntax
    import('web-vitals').then(({ getCLS, getFID, getFCP, getLCP, getTTFB }) => {
      // Call the web-vitals functions and pass the callback function as their parameter
      getCLS(onPerfEntry);
      getFID(onPerfEntry);
      getFCP(onPerfEntry);
      getLCP(onPerfEntry);
      getTTFB(onPerfEntry);
    });
  }
};

// Export the reportWebVitals function as the default export of this module
export default reportWebVitals;
