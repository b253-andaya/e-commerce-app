// This line of code imports the '@testing-library/jest-dom' library into the Jest testing environment.
// This library provides additional custom Jest matchers that can be used to test the DOM elements of a React application.
import '@testing-library/jest-dom';
