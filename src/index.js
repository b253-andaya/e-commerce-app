// Import React and ReactDOM
import React from 'react';
import ReactDOM from 'react-dom/client';
// Import CSS styles
import './index.css';
// Import main App component
import App from './App';
// Import function to measure web vitals
import reportWebVitals from './reportWebVitals';
// Import store for Redux
import store from './store';
// Import provider and persist gate for Redux state persistence
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import persistStore from 'redux-persist/lib/persistStore';

// Create a persisted store with the persistStore function
const persistedStore = persistStore(store);

// Create a root for React to render the App component into
const root = ReactDOM.createRoot(document.getElementById('root'));
// Render the App component wrapped in a Provider and PersistGate
root.render(
  <Provider store={store}>
    <React.StrictMode>
      <PersistGate loading={<div>Loading...</div>} persistor={persistedStore}>
        <App />
      </PersistGate>
    </React.StrictMode>
  </Provider>
);

// Call the reportWebVitals function to measure performance
reportWebVitals();
