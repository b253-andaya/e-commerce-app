// Import the necessary functions from the testing-library/react library
import { render, screen } from '@testing-library/react';
// Import the App component
import App from './App';

test('renders learn react link', () => {
  // Define a test case with a description
  render(<App />); // Render the App component
  // Find an element that contains the text "learn react" (case-insensitive) using the screen object from the testing-library/react library
  const linkElement = screen.getByText(/learn react/i);
  // Assert that the linkElement is in the document (i.e., it exists)
  expect(linkElement).toBeInTheDocument();
});
