// Import the App.css file for styling
import './App.css';
// Import Bootstrap CSS for pre-defined styles
import 'bootstrap/dist/css/bootstrap.min.css';
// Import necessary components from react-router-dom
import { BrowserRouter, Route, Routes } from 'react-router-dom';
// Import Navigation component
import Navigation from './components/Navigation';
// Import Home page component
import Home from './pages/Home';
// Import Login page component
import Login from './pages/Login';
// Import Signup page component
import Signup from './pages/Signup';
import { useSelector } from 'react-redux';
import NewProduct from './pages/NewProduct';
import ProductPage from './pages/ProductPage';
import CategoryPage from './pages/CategoryPage';
import ScrollToTop from './components/ScrollToTop';
import CartPage from './pages/CartPage';
import OrdersPage from './pages/OrdersPage';
import AdminDashboard from './pages/AdminDashboard';

// Define the App component
function App() {
  const user = useSelector((state) => state.user);
  return (
    <div className="App">
      <BrowserRouter>
        <ScrollToTop />
        <Navigation />
        <Routes>
          <Route index element={<Home />} />
          {!user && (
            <>
              <Route path="/login" element={<Login />} />
              <Route path="/signup" element={<Signup />} />
            </>
          )}

          {user && (
            <>
              <Route path="/cart" element={<CartPage />} />
              <Route path="/orders" element={<OrdersPage />} />
            </>
          )}
          {user && user.isAdmin && (
            <>
              <Route path="/admin" element={<AdminDashboard />} />
              {/* <Route path="/product/:id/edit" element={<EditProductPage />} /> */}
            </>
          )}
          <Route path="/product/:id" element={<ProductPage />} />
          <Route path="/category/:category" element={<CategoryPage />} />

          <Route path="/new-product" element={<NewProduct />} />

          <Route path="*" element={<Home />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

// Export the App component for use in other parts of the application
export default App;
