// This is an import statement that imports the createSlice function from the @reduxjs/toolkit library.
import { createSlice } from '@reduxjs/toolkit';

// appApi
// Imports the appApi function from the ../services/appApi file.
import appApi from '../services/appApi';

// Creates a constant variable initialState and initializes it to an empty array.
const initialState = null;

// Creates a slice of the Redux store called userSlice using the createSlice function
// Reducers property is an empty object, indicating that no reducers are defined for this slice.
export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    logout: () => initialState,
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      appApi.endpoints.signup.matchFulfilled,
      (_, { payload }) => payload
    );
    builder.addMatcher(
      appApi.endpoints.login.matchFulfilled,
      (_, { payload }) => payload
    );
    builder.addMatcher(
      appApi.endpoints.addToCart.matchFulfilled,
      (_, { payload }) => payload
    );
    builder.addMatcher(
      appApi.endpoints.removeFromCart.matchFulfilled,
      (_, { payload }) => payload
    );
    builder.addMatcher(
      appApi.endpoints.increaseCartProduct.matchFulfilled,
      (_, { payload }) => payload
    );
    builder.addMatcher(
      appApi.endpoints.decreaseCartProduct.matchFulfilled,
      (_, { payload }) => payload
    );
    builder.addMatcher(
      appApi.endpoints.createOrder.matchFulfilled,
      (_, { payload }) => payload
    );
  },
});

// Objects that describe the type of the action and any payload data associated with it. These actions are then dispatched to the Redux store to trigger updates to the application state.
export const { logout } = userSlice.actions;

// Update the state in response to actions dispatched to the Redux store
export default userSlice.reducer;
